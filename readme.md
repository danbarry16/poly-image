*Poly Image* is a simple Java based command-line utility to create
polygon-based images from raw images. It uses a genetic algorithm to
incrementally improve the output SVG and produce an SVG.

# Building

To build, you will need the `ant` Java build system and can simply run:

```
ant
```

# Test

A test image from [Wiki
Commons](https://commons.wikimedia.org/wiki/File:Red_Arrow_RIAT.jpg), showing a
Royal Air Force Red Arrow aircraft.

![Original Red Arrows test image](test/640px-Red_Arrow_RIAT.jpg)

![Polygon'd Red Arrows image](test/640px-Red_Arrow_RIAT.png)

This was produced with the following command line option (takes a few minutes
to run):

```
java -jar dist/poly.jar \
  -o color FFFFFF \
  -o pop 128 \
  -o sel 0.25 \
  -o mut 0.1 \
  -o runs 4096 \
  -p 300 \
  -s 0.5 \
  test/640px-Red_Arrow_RIAT.jpg \
  plane.svg
```

And then converted to an image like so:

```
inkscape --without-gui --export-png=test/640px-Red_Arrow_RIAT.png -w 640 plane.svg
```

# Improvement Ideas

## `AlgGA.java`

* [ ] Post clean-up to remove redundant layers, namely layers that cannot be
seen.

## (New) `AlgGAMatrix.java`

* [ ] Pre-place the polygons in a matrix, each polygon joined by a face to
another polygon, with some polygons sharing a face with the edge. These should
be evenly distributed.
* [ ] Iteratively adjust the position of the shared points using a genetic
algorithm.

package b.pi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * SVG.java
 *
 * Basic structure of an SVG.
 **/
public class SVG{
  private class Poly{
    private int[][] points;
    private int color;

    /**
     * Poly()
     *
     * Store the polygon properties.
     *
     * @param points The points of the polygon.
     * @param color The colour of the polygon.
     **/
    public Poly(int[][] points, int color){
      this.points = points;
      this.color = color;
    }

    /**
     * getPoints()
     *
     * Get the points in the raw format.
     *
     * @return The raw format points.
     **/
    public int[][] getPoints(){
      return points;
    }

    /**
     * getXPoints()
     *
     * Get the X points.
     *
     * @return Return only the X points.
     **/
    public int[] getXPoints(){
      int[] p = new int[points.length];
      for(int i = 0; i < points.length; i++){
        p[i] = points[i][0];
      }
      return p;
    }

    /**
     * getYPoints()
     *
     * Get the Y points.
     *
     * @return Return only the Y points.
     **/
    public int[] getYPoints(){
      int[] p = new int[points.length];
      for(int i = 0; i < points.length; i++){
        p[i] = points[i][1];
      }
      return p;
    }

    /**
     * getColor()
     *
     * Get the colour of this polygon.
     *
     * @return The colour of this polygon.
     **/
    public int getColor(){
      return color;
    }

    @Override
    public String toString(){
      StringBuilder sb = new StringBuilder("<polygon points=\"");
      for(int x = 0; x < points.length; x++){
        if(x > 0){
          sb.append(" ");
        }
        for(int y = 0; y < points[x].length; y++){
          if(y > 0){
            sb.append(",");
          }
          sb.append(Integer.toString(points[x][y]));
        }
      }
      sb.append("\" style=\"fill:" + intToHTML(color) + ";\" />");
      return sb.toString();
    }
  }

  private int width;
  private int height;
  private int color;
  private ArrayList<Poly> polys;

  /**
   * SVG()
   *
   * Constructor to create a bank SVG of a given size.
   *
   * @param width The width of the SVG.
   * @param height The height of the SVG.
   * @param color Background colour of SVG.
   **/
  public SVG(int width, int height, int color){
    this.width = width;
    this.height = height;
    this.color = color;
    polys = new ArrayList<Poly>();
  }

  /**
   * addPoly()
   *
   * Add a new polygon to the image.
   *
   * @param polys The polygon points. The points do not need to be joined.
   * @param color The colour to apply to the polygons.
   * @return The instance of this SVG object.
   **/
  public SVG addPoly(int[][] polys, int color){
    this.polys.add(new Poly(polys, color));
    return this;
  }

  /**
   * setPoly()
   *
   * Set a polygon at a given location.
   *
   * @param x The index to be replaced.
   * @param polys The polygon points. The points do not need to be joined.
   * @param color The colour to apply to the polygons.
   * @return The instance of this SVG object.
   **/
  public SVG setPoly(int x, int[][] polys, int color){
    this.polys.set(x, new Poly(polys, color));
    return this;
  }

  /**
   * swapPoly()
   *
   * Swap the location of two polygons, therefore the layering.
   *
   * @param x The location of one poly.
   * @param y The location of another poly.
   * @return The instance of this SVG object.
   **/
  public SVG swapPoly(int x, int y){
    Poly p = polys.get(x);
    polys.set(x, polys.get(y));
    polys.set(y, p);
    return this;
  }

  /**
   * getPoly()
   *
   * Get a given polygon.
   *
   * @param x The location to retrieve the polygon from.
   * @return The raw arrays.
   **/
  public int[][] getPoly(int x){
    /* Deep clone polys */
    int[][] a = polys.get(x).getPoints();
    int[][] b = new int[a.length][];
    for(int i = 0; i < a.length; i++){
      b[i] = new int[a[i].length];
      for(int j = 0; j < a[i].length; j++){
        b[i][j] = a[i][j];
      }
    }
    return b;
  }

  /**
   * getPolyCol()
   *
   * Get the colour of a given polygon.
   *
   * @param x The location to retrieve the polygon from.
   * @return The raw colour integer.
   **/
  public int getPolyCol(int x){
    return polys.get(x).getColor();
  }

  /**
   * polyLength()
   *
   * Get the number of polygons in this SVG.
   *
   * @return The number of polygons.
   **/
  public int polyLength(){
    return polys.size();
  }

  /**
   * intToHTML()
   *
   * Integer to hexadecimal HTML code conversion.
   *
   * @param c The colour value.
   * @return The converted value.
   **/
  private static String intToHTML(int c){
    StringBuilder h = new StringBuilder("#");
    for(int o = 24 - 4; o >= 0; o -= 4){
      h.append(intToHex((c >> o) & 0xF));
    }
    return h.toString();
  }

  /**
   * intToHex()
   *
   * Convert a number between 0 and 15 to a hex character.
   *
   * @param i The value to be converted.
   * @return The converted hex character.
   **/
  private static char intToHex(int i){
    if(i >= 0){
      if(i < 10){
        return (char)('0' + i);
      }else if(i < 16){
        return (char)('A' + (i - 10));
      }
    }
    return '?';
  }

  @Override
  public String toString(){
    StringBuilder sb =
      new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
    sb.append("<svg width=\"");
      sb.append(width);
      sb.append("\" height=\"");
      sb.append(height);
      sb.append("\" xmlns=\"http://www.w3.org/2000/svg\">");
    for(int x = 0; x < polys.size(); x++){
      sb.append(polys.get(x).toString());
    }
    sb.append("</svg>");
    return sb.toString();
  }

  /**
   * toBufferedImage()
   *
   * Convert this SVG to a buffered image representation.
   *
   * @return The BufferedImage representation of this SVG.
   **/
  public BufferedImage toBufferedImage(){
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics g = bi.getGraphics();
    /* Draw background colour */
    g.setColor(new Color(color));
    g.fillRect(0, 0, width, height);
    /* Draw polygons */
    for(int x = 0; x < polys.size(); x++){
      Poly p = polys.get(x);
      g.setColor(new Color(p.getColor()));
      g.fillPolygon(p.getXPoints(), p.getYPoints(), p.getPoints().length);
    }
    return bi;
  }
}

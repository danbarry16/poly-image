package b.pi;

import java.util.HashMap;

/**
 * Main.java
 *
 * Process command line parameters and execute the program as required.
 **/
public class Main{
  private HashMap<String, Object> params;

  /**
   * main()
   *
   * This is the main program entry point, its creates an object instance and
   * processes the command line parameters there.
   *
   * @param args The command line arguments.
   **/
  public static void main(String[] args){
    new Main(args);
  }

  /**
   * Main()
   *
   * Process the command line arguments and execute the program as required.
   *
   * @param args The command line arguments.
   **/
  public Main(String[] args){
    /* Initial variable values */
    params = new HashMap<String, Object>();
    params.put("alg", "ga");
    params.put("pIn", null);
    params.put("pOut", null);
    params.put("polygons", 300);
    params.put("scale", 1.0);
    /* Process options */
    for(int x = 0; x < args.length; x++){
      switch(args[x]){
        case "-a" :
        case "--alg" :
          x = alg(args, x);
          break;
        case "-h" :
        case "--help" :
          x = help(args, x);
          break;
        case "-o" :
        case "--override" :
          x = override(args, x);
          break;
        case "-p" :
        case "--polygons" :
          x = polygons(args, x);
          break;
        case "-s" :
        case "--scale" :
          x = scale(args, x);
          break;
        default :
          if(x == args.length - 2){
            params.put("pIn", args[x]);
          }else if(x == args.length - 1){
            params.put("pOut", args[x]);
          }else{
            System.err.println("Invalid input '" + args[x] + "'");
            /* Reset, we have no idea what the user wanted */
            params.put("pIn", null);
            params.put("pOut", null);
            /* Break out of loop */
            x = args.length;
          }
          break;
      }
    }
    /* How should we continue? */
    if(params.get("pIn") != null && params.get("pOut") != null){
      /* Output input parameters */
      System.err.println("Input parameters read are as follows:");
      for(String key : params.keySet()){
        System.err.println("  " + key + " -> " + params.get(key));
      }
      /* Process images */
      Process proc = new Process(params);
      /* Make sure parameters were good */
      if(!proc.isReady()){
        System.err.println("Bad input parameters to begin processing");
        System.exit(1);
      }
      /* Begin processing */
      proc.run();
    }else{
      System.err.println("See '--help' for more details");
      System.exit(1);
    }
  }

  /**
   * alg()
   *
   * Set the algorithm.
   *
   * @param args The command line arguments.
   * @param x The current offset into the parameters.
   * @return The new offset into the command line parameters.
   **/
  public int alg(String[] args, int x){
    if(++x < args.length){
      params.put("alg", args[x]);
    }else{
      System.err.println("Missing algorithm parameter");
      System.exit(1);
    }
    return x;
  }

  /**
   * help()
   *
   * Display the help.
   *
   * @param args The command line arguments.
   * @param x The current offset into the parameters.
   * @return The new offset into the command line parameters.
   **/
  public int help(String[] args, int x){
    System.out.println("java -jar poly.jar [OPT]");
    System.out.println("java -jar poly.jar [OPT] <IN> <OUT>");
    System.out.println("");
    System.out.println("  OPTions");
    System.out.println("");
    System.out.println("    -a  --alg       Select the algorithm");
    System.out.println("                      <STR> The algorithm to be used");
    System.out.println("                        ga  Genetic algorithm (default)");
    System.out.println("    -h  --help      Display this help");
    System.out.println("    -o  --override  Change an algorithm variable");
    System.out.println("                      'ga' (Genetic Algorithm):");
    System.out.println("                        color  Background colour (hex)");
    System.out.println("                        pop    Size of population");
    System.out.println("                        sel    Breeding ratio (0 to 1)");
    System.out.println("                        mut    Mutation rate (0 to 1)");
    System.out.println("                        runs   Total number of runs");
    System.out.println("    -p  --polygons  Select the algorithm");
    System.out.println("                      <DBL> Scale the input image");
    System.out.println("    -s  --scale     Select the algorithm");
    System.out.println("                      <DBL> Scale the input image");
    System.out.println("");
    System.out.println("  INput");
    System.out.println("");
    System.out.println("    The input image file. It must be an image type");
    System.out.println("    supported by BufferedImage, i.e. JPG or PNG for");
    System.out.println("    example.");
    System.out.println("");
    System.out.println("  OUTput");
    System.out.println("");
    System.out.println("    The output image file. This will be an SVG.");
    System.exit(0);
    return x;
  }

  /**
   * override()
   *
   * Override an algorithm parameter.
   *
   * @param args The command line arguments.
   * @param x The current offset into the parameters.
   * @return The new offset into the command line parameters.
   **/
  public int override(String[] args, int x){
    x += 2;
    if(x < args.length){
      switch(args[x - 1]){
        case "color" :
          params.put(args[x - 1], parseVal("hex", args[x]));
          break;
        case "pop" :
          params.put(args[x - 1], parseVal("int", args[x]));
          break;
        case "sel" :
          params.put(args[x - 1], parseVal("double", args[x]));
          break;
        case "mut" :
          params.put(args[x - 1], parseVal("double", args[x]));
          break;
        case "runs" :
          params.put(args[x - 1], parseVal("int", args[x]));
          break;
        default :
          System.err.println("Unknown override parameter '" + args[x - 1] + "'");
          System.exit(1);
          break;
      }
    }else{
      System.err.println("Missing override parameters");
      System.exit(1);
    }
    return x;
  }

  /**
   * parseVal()
   *
   * Parse a raw value from the command line.
   *
   * @param type The type to convert the value to.
   * @param val The value to be converted.
   * @return The object to be added, otherwise NULL on error.
   **/
  private static Object parseVal(String type, String val){
    try{
      switch(type){
        case "byte" :
          return Byte.parseByte(val);
        case "char" :
          return new Character((char)Integer.parseInt(val));
        case "double" :
          return Double.parseDouble(val);
        case "float" :
          return Float.parseFloat(val);
        case "hex" :
          return Integer.parseInt(val, 16);
        case "int" :
          return Integer.parseInt(val);
        case "long" :
          return Long.parseLong(val);
        case "string" :
          return val;
        default :
          System.err.println("Unsupported type '" + type + "'");
          return null;
      }
    }catch(Exception e){
      System.err.println("Failed to parse '" + val + "' as " + type);
    }
    return null;
  }

  /**
   * polygons()
   *
   * Set the number of polygons to be used.
   *
   * @param args The command line arguments.
   * @param x The current offset into the parameters.
   * @return The new offset into the command line parameters.
   **/
  public int polygons(String[] args, int x){
    if(++x < args.length){
      try{
        params.put("polygons", Integer.parseInt(args[x]));
      }catch(NumberFormatException e){
        System.err.println("Incorrect polygon parameter");
        System.exit(1);
      }
    }else{
      System.err.println("Missing polygon parameter");
      System.exit(1);
    }
    return x;
  }

  /**
   * scale()
   *
   * Set the scale to be used.
   *
   * @param args The command line arguments.
   * @param x The current offset into the parameters.
   * @return The new offset into the command line parameters.
   **/
  public int scale(String[] args, int x){
    if(++x < args.length){
      try{
        params.put("scale", Double.parseDouble(args[x]));
      }catch(NumberFormatException e){
        System.err.println("Incorrect scale parameter");
        System.exit(1);
      }
    }else{
      System.err.println("Missing scale parameter");
      System.exit(1);
    }
    return x;
  }
}

package b.pi;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

/**
 * AlgGA.java
 *
 * A basic genetic algorithm to generate the output image.
 **/
public class AlgGA extends Alg{
  private double MAX_ERROR;
  private boolean ready = false;
  private BufferedImage orig;
  private int width;
  private int height;
  private int color;
  private int polys;
  private int curPolys;
  private int pop;
  private int sel;
  private int mut;
  private int runs;
  private int curGen;
  private Random rand;
  private SVG best;
  private long bestFit;
  private long avgFit;
  private SVG[] population;

  /**
   * AlgGA()
   *
   * Setup the genetic algorithm ready to be run.
   *
   * @param orig The original image to be used in comparing for the fitness
   * value.
   * @param color The background colour of the image.
   * @param polys The number of polygons to be used.
   * @param pop How large the population should be.
   * @param sel The number of the population to be selected for breeding, where
   * 0.0 is none and 1.0 is all.
   * @param mut The number of mutations to introduce into the population, where
   * 0.0 is none and 1.0 is mutate everything.
   * @param runs How many generations to run for.
   **/
  public AlgGA(
    BufferedImage orig,
    int color,
    int polys,
    int pop,
    float sel,
    float mut,
    int runs
  ){
    ready = false;
    /* Store important variables */
    this.orig = orig;
    width = orig.getWidth();
    if(width < 1){
      System.err.println("Image width is less than one pixel");
      return;
    }
    height = orig.getHeight();
    if(height < 1){
      System.err.println("Image height is less than one pixel");
      return;
    }
    this.color = color;
    this.polys = polys;
    if(polys < 1){
      System.err.println("There must be at least one polygon");
      return;
    }
    this.pop = pop;
    if(pop < 2){
      System.err.println("Population too small, need at least two to tango");
      return;
    }
    this.sel = (int)(pop * sel);
    if(sel <= 0.0f || sel >= 1.0f){
      System.err.println("Selection size should be >= 0.0 and <= 1.0");
      return;
    }
    this.mut = (int)(pop * mut);
    if(mut <= 0.0f || mut >= 1.0f){
      System.err.println("Mutation size should be >= 0.0 and <= 1.0");
      return;
    }
    this.runs = runs;
    /* Setup internal state */
    MAX_ERROR = Math.log(Math.sqrt(3 * Math.pow(256, 2))) * width * height;
    curPolys = 1;
    curGen = 0;
    rand = new Random(0);
    ready = true;
    /* Setup initial population */
    population = new SVG[pop];
    for(int x = 0; x < pop; x++){
      SVG s = new SVG(width, height, color);
      /* Add random polys */
      for(int y = 0; y < curPolys; y++){
        /* Generate and add it */
        int i = rand.nextInt(width);
        int j = rand.nextInt(height);;
        s.addPoly(randomPolygon(i, j), orig.getRGB(i, j));
      }
      population[x] = s;
    }
    best = population[0];
    bestFit = Long.MAX_VALUE;
  }

  @Override
  public boolean isReady(){
    return ready;
  }

  @Override
  public float run(){
    /* Generate fitness levels (we don't want to keep doing this) */
    long[] curFit = processFitness();
    /* Select best based on fitness */
    SVG[] curBest = processBest(curFit);
    /* Breed the best partners */
    breedPopulation(curBest);
    /* Mutate them using the mutation rate */
    curPolys = (int)(polys * ((double)curGen / (double)runs)) + 1;
    mutatePopulation();
    /* Proper progress report */
    System.err.println(
      "Polys: " +
      curPolys +
      "\tError: " +
      ((bestFit / MAX_ERROR) * 100) +
      " %" +
      "\tAverage error: " +
      ((avgFit / MAX_ERROR) * 100) +
      " %"
    );
    /* If there is very little error are done here */
    if(bestFit / MAX_ERROR < 0.001){
      System.err.println("Good error rate reached, ending");
      return 1.0f;
    /* Return progress */
    }else{
      ++curGen;
      return (float)((double)curGen / (double)runs);
    }
  }

  /**
   * processFitness()
   *
   * Process the fitness ratings for each member of the population.
   *
   * @return A list of the lowest fitness values.
   **/
  private long[] processFitness(){
    avgFit = 0;
    long[] curFit = new long[population.length];
    for(int i = 0; i < population.length; i++){
      curFit[i] = calculateFitness(population[i]);
      avgFit += curFit[i];
    }
    avgFit /= population.length;
    return curFit;
  }

  /**
   * calculateFitness()
   *
   * Calculate the fitness of a given SVG when compared to the original image.
   *
   * @return The current fitness value compared to the original image. Lower is
   * better (less difference).
   **/
  private long calculateFitness(SVG s){
    BufferedImage p = s.toBufferedImage();
    long diff = 0;
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int co = orig.getRGB(x, y);
        int ro = (co >> 16) & 0xFF;
        int go = (co >>  8) & 0xFF;
        int bo = (co      ) & 0xFF;
        int cp = p.getRGB(x, y);
        int rp = (cp >> 16) & 0xFF;
        int gp = (cp >>  8) & 0xFF;
        int bp = (cp      ) & 0xFF;
        double d = Math.sqrt(
          Math.pow(ro - rp, 2) +
          Math.pow(go - gp, 2) +
          Math.pow(bo - bp, 2)
        );
        if(d != 0){
          diff += Math.log(d);
        }
      }
    }
    return diff;
  }

  /**
   * processBest()
   *
   * Find the best SVGs ready for breeding.
   *
   * @param curFit The current fitness values array for each of the SVGs in the
   * population.
   * @return The current best list of SVGs.
   **/
  private SVG[] processBest(long[] curFit){
    /* TODO: Replace this slow algorithm. */
    SVG[] curBest = new SVG[sel];
    /* Make sure the current best gets into the population */
    curBest[0] = best;
    /* Keep going until we select enough for breeding */
    for(int i = 1; i < sel; i++){
      /* Find the best left in the list */
      int bi = 0;
      long bf = Long.MAX_VALUE;
      for(int z = 0; z < population.length; z++){
        if(curFit[z] <= bf){
          bi = z;
          bf = curFit[z];
        }
      }
      /* See if we beat the global best fitness */
      if(i == 1 && bf <= bestFit){
        /* Save the best we found so far */
        best = new SVG(width, height, color);
        for(int x = 0; x < curPolys; x++){
          best.addPoly(population[bi].getPoly(x), population[bi].getPolyCol(x));
        }
        bestFit = bf;
      }
      curBest[i] = population[bi];
      curFit[bi] = Long.MAX_VALUE;
    }
    /* Return the list of best SVGs */
    return curBest;
  }

  /**
   * breedPopulation()
   *
   * Breed parents together and create a new population from their children.
   *
   * @param curBest An array of the current best parents.
   **/
  private void breedPopulation(SVG[] curBest){
    /* Fill the population with children */
    for(int i = 0; i < population.length; i++){
      /* Select two parents */
      SVG a = curBest[rand.nextInt(curBest.length)];
      SVG b;
      /* Don't breed with yourself */
      while((b = curBest[rand.nextInt(curBest.length)]) == a);
      /* Select random place to splice their genomes */
      int s = rand.nextInt(curPolys);
      /* Create new child */
      SVG c = new SVG(width, height, color);
      for(int z = 0; z < curPolys; z++){
        if(z < s){
          c.addPoly(a.getPoly(z), a.getPolyCol(z));
        }else{
          c.addPoly(b.getPoly(z), b.getPolyCol(z));
        }
      }
      /* Insert child into population */
      population[i] = c;
    }
  }

  /**
   * mutatePopulation()
   *
   * Add randomness to the current population.
   **/
  private void mutatePopulation(){
    /* Randomize polygons */
    for(int z = 0; z < population.length; z++){
      /* Add a polygon if required */
      if(population[z].polyLength() < curPolys){
        /* Add a new random polygon */
        int[][] points = randomPolygon(rand.nextInt(width), rand.nextInt(height));
        int[] c = centrePoly(points);
        int color = orig.getRGB(c[0], c[1]);
        population[z].addPoly(points, color);
      }
      /* Perform minor mutations */
      int[] mLayers = new int[]{
        population[z].polyLength() - 1,
        rand.nextInt(population[z].polyLength()),
        rand.nextInt(population[z].polyLength())
      };
      for(int x : mLayers){
        /* Set mutation rate (latest polygon most active) */
        double m = 0.05;
        /* Generate a mutated polygon */
        int[][] points = mutatePoly(population[z].getPoly(x), m);
        int[] c = centrePoly(points);
        int color = orig.getRGB(c[0], c[1]);
        /* Set it */
        population[z].setPoly(x, points, color);
      }
    }
    /* Make sure best contains the correct number of polygons */
    if(best.polyLength() < curPolys){
      /* Copy last polygon to play it safe */
      int[][] points = best.getPoly(best.polyLength() - 1);
      int color = best.getPolyCol(best.polyLength() - 1);
      best.addPoly(points, color);
    }
  }

  /**
   * mutatePoly()
   *
   * Mutate a given polygon by the global mutation.
   *
   * @param p The polygon to be mutated.
   * @param d Mutation divisor.
   * @return The mutated polygon.
   **/
  private int[][] mutatePoly(int[][] p, double d){
    if(d <= 0.0){
      return p;
    }
    double w = (double)width;
    double h = (double)height;
    for(int y = 0; y < p.length; y++){
      if(rand.nextFloat() < mut){
        p[y][0] = clamp(
          (int)(
            p[y][0] + rand.nextInt((int)(w * d)) - (w * d / 2.0)
          ),
          0,
          width - 1
        );
      }
      if(rand.nextFloat() < mut){
        p[y][1] = clamp(
          (int)(
            p[y][1] + rand.nextInt((int)(h * d)) - (h * d / 2.0)
          ),
          0,
          height - 1
        );
      }
    }
    return p;
  }

  private static int clamp(int val, int min, int max){
    return Math.max(min, Math.min(max, val));
  }

  /**
   * centrePoly()
   *
   * Get the centre of a given polygon.
   *
   * @param p The polygon to be calculated.
   * @return The centre of the polygon.
   **/
  private int[] centrePoly(int[][] p){
    int[] c = new int[]{ 0, 0 };
    for(int y = 0; y < p.length; y++){
      c[0] += p[y][0];
      c[1] += p[y][1];
    }
    c[0] /= p.length;
    c[1] /= p.length;
    return c;
  }

  /**
   * randomPolygon()
   *
   * Create a small polygon based around some centre point.
   *
   * @param x The X coordinate for the centre point.
   * @param y The Y coordinate for the centre point.
   * @return The calculated polygon.
   **/
  private int[][] randomPolygon(int x, int y){
    /* TODO: Distance limiting should be a setting. */
    int r = (width + height) / 8;
    int r_2 = r / 2;
    int[][] p = new int[3][];
    for(int i = 0; i < p.length; i++){
      p[i] = new int[2];
      p[i][0] = Math.min(width - 1, Math.max(0, x + (rand.nextInt(r) - r_2)));
      p[i][1] = Math.min(height - 1, Math.max(0, y + (rand.nextInt(r) - r_2)));
    }
    return p;
  }

  @Override
  public SVG getSVG(){
    return best;
  }
}

package b.pi;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 * Process.java
 *
 * Process the input image with the provided parameters.
 **/
public class Process{
  private boolean ready = false;
  private BufferedImage orig;
  private File output;
  private Alg alg;

  /**
   * Process()
   *
   * Load the parameters and check they are suitable.
   *
   * @param params The parameters to be used.
   **/
  public Process(HashMap<String, Object> params){
    /* Reset the ready state and internal variables */
    ready = false;
    orig = null;
    output = null;
    this.alg = null;
    /* Check input image */
    try{
      orig = ImageIO.read(new File((String)params.get("pIn")));
    }catch(IOException e){
      System.err.println("Failed to open image");
      return;
    }
    /* Scale input image */
    double scale = (Double)params.get("scale");
    BufferedImage temp = new BufferedImage(
      (int)(orig.getWidth() * scale),
      (int)(orig.getHeight() * scale),
      BufferedImage.TYPE_INT_ARGB
    );
    AffineTransform at = new AffineTransform();
    at.scale(scale, scale);
    AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
    temp = scaleOp.filter(orig, temp);
    orig = temp;
    /* Check output image */
    output = new File((String)params.get("pOut"));
    if(output.exists()){
      System.err.println("Output file already exists");
      return;
    }
    if(output.isDirectory()){
      System.err.println("Output is a directory");
      return;
    }
    /* Check algorithm */
    switch((String)params.get("alg")){
      case "ga" :
        this.alg = new AlgGA(
          orig,
          params.get("color") != null ? (Integer)params.get("color")             : 0xFFFFFF,
                                        (Integer)params.get("polygons"),
          params.get("pop")   != null ? (Integer)params.get("pop")               : 256,
          params.get("sel")   != null ? ((Double)params.get("sel")).floatValue() : 0.25f,
          params.get("mut")   != null ? ((Double)params.get("mut")).floatValue() : 0.1f,
          params.get("runs")  != null ? (Integer)params.get("runs")              : 65536
        );
        break;
      default :
        System.err.println("Unknown algorithm '" + alg + "'");
        return;
    }
    /* If we haven't yet errored, we should be good */
    ready = true;
  }

  /**
   * isReady()
   *
   * Check whether the algorithm is ready to run given the previous parameters.
   *
   * @return True if ready to begin processing, otherwise false.
   **/
  public boolean isReady(){
    return ready;
  }

  /**
   * run()
   *
   * Blocking call to process the image until completion.
   **/
  public void run(){
    /* Run until the problem is complete */
    float progress = 0.0f;
    long lastUpdate = System.currentTimeMillis() + 1000;
    while((progress = alg.run()) != 1.0f){
      if(System.currentTimeMillis() >= lastUpdate){
        System.err.println((progress * 100.0f) + " %");
        lastUpdate = System.currentTimeMillis() + 1000;
        try{
         writeOutputSVG(new File(output.toString() + "-" + (int)(progress * 100) + ".svg"), alg.getSVG());
          ImageIO.write(alg.getSVG().toBufferedImage(), "jpg", new File(output + "-" + (int)(progress * 100) + ".jpg"));
        }catch(IOException e){
          /* Do nothing */
        }
      }
    }
    writeOutputSVG(output, alg.getSVG());
  }

  /**
   * writeOutputSVG()
   *
   * Write the output SVG for the user.
   *
   * @param o The output filename.
   * @param s The SVG to be written.
   **/
  private void writeOutputSVG(File o, SVG s){
    /* Write output */
    try{
      BufferedWriter bw = new BufferedWriter(new FileWriter(o));
      bw.write(s.toString());
      bw.close();
    }catch(IOException e){
      /* No options left, may as well tell the user why this failed */
      e.printStackTrace();
      System.exit(1);
    }
  }
}

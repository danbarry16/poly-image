package b.pi;

/**
 * Alg.java
 *
 * Define the basic properties of an algorithm to be used.
 **/
public abstract class Alg{
  /**
   * isReady()
   *
   * Check whether the algorithm was setup correctly.
   *
   * @return True if the algorithm is ready to run, otherwise false.
   **/
  public boolean isReady(){
    return true;
  }

  /**
   * run()
   *
   * Run some portion of the processing problem and return an estimate of how
   * much total progress has been made so far.
   *
   * @return An estimation of the total progress made so far, 0.0 meaning no
   * progress and 1.0 meaning the problem is complete.
   **/
  public float run(){
    return 1.0f;
  }

  /**
   * getSVG()
   *
   * Get the current best SVG the algorithm has been able to generate.
   *
   * @return The current best SVG.
   **/
  public SVG getSVG(){
    return new SVG(256, 256, 0xFFFFFF);
  }
}
